__all__ = ["SocketQuestion", "INSTALL", "REMOVE", "CONFIG", "CUSTOM"]

INSTALL = "install"
REMOVE = "remove"
CONFIG = "config"
CUSTOM = "custom"
import httpimport

class SocketQuestion:
    def __init__(self):
        self.TYPE = None
        self.QUERY = None
        self.FRONTEND = None
        self.BACKEND = None

    def compile(self):
        return self.__dict__
