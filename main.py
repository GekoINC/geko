import argparse
import SocketDriver
import os
import socket
__version__="0.0.1"
__backend__="0.0.1"

"""if os.geteuid() != 0:
    exit("You need to have root privileges to run this script.\nPlease try again, this time using 'sudo'. Exiting.")
"""

sock_question = []

arg = argparse.ArgumentParser(prog="Geko", description="Geko .deb package archive")
arg.add_argument("-i", "--install", help="Install a repo", action="store", nargs="+")
arg.add_argument("-u", "--update", help="Check for a system update and install it", action="store_true")
arg.add_argument("-r", "--remove", help="Remove a repo", action="store", nargs="+")
arg.add_argument("-c", "--config", help="Change a setting", action="store", nargs="+")
arg.add_argument("--verbose", help=argparse.SUPPRESS, action="store_true")
arg.add_argument("--one-time-driver", help=argparse.SUPPRESS, action="store", default="dpkg")
arg.add_argument("--beta-gekyml", help=argparse.SUPPRESS, action="store_true")
args = arg.parse_args()

if args.beta_gekyml_compile:
    exit(f"Gekyml is not avalible on Geko version {__version__}!\nGekyml planned release version 1.0.0")

def parseConfigArg(config):
    newConfig = {}
    for entry in config:
        x = entry.split("=")
        newConfig[x[0]] = x[1]
    return newConfig

if args.config != None:
    config = parseConfigArg(args.config)
    q = SocketDriver.SocketQuestion()
    q.TYPE = SocketDriver.CONFIG
    q.QUERY = config
    q.FRONTEND = __version__
    q.BACKEND = __backend__
    sock_question.append(q.compile())


if args.install != None:
    q = SocketDriver.SocketQuestion()
    q.TYPE = SocketDriver.INSTALL
    q.QUERY = args.install
    q.FRONTEND = __version__
    q.BACKEND = __backend__
    sock_question.append(q.compile())

if args.remove != None:
    q = SocketDriver.SocketQuestion()
    q.TYPE = SocketDriver.REMOVE
    q.QUERY = args.remove
    q.FRONTEND = __version__
    q.BACKEND = __backend__
    sock_question.append(q.compile())

if args.update != None:
    q = SocketDriver.SocketQuestion()
    q.TYPE = SocketDriver.CUSTOM
    q.QUERY = "UPDATE"
    q.FRONTEND = __version__
    q.BACKEND = __backend__
    sock_question.append(q.compile())

if args.verbose != None:
        q = SocketDriver.SocketQuestion()
        q.TYPE = SocketDriver.CUSTOM
        q.QUERY = "VERBOSE"
        q.FRONTEND = __version__
        q.BACKEND = __backend__
        sock_question.append(q.compile())


if args.one_time_driver != None:
    q = SocketDriver.SocketQuestion()
    q.TYPE = SocketDriver.CUSTOM
    q.QUERY = {"OTD": args.one_time_driver}
    q.FRONTEND = __version__
    q.BACKEND = __backend__
    sock_question.append(q.compile())


def sendSock(q):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect(("127.0.0.1", 9324))
        s.sendall(q)
        data = s.recv(1024)
    return data


print(sock_question)
print(args)