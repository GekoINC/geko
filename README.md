# Geko
Geko is a package libary powered by dpkg.

## Install
Geko is in beta, You cannot install it

## Help

# Main commands
```
usage: Geko [-h] [-i INSTALL [INSTALL ...]] [-u] [-r REMOVE [REMOVE ...]] [-c CONFIG [CONFIG ...]]

Geko .deb package archive

optional arguments:
  -h, --help            show this help message and exit
  -i INSTALL [INSTALL ...], --install INSTALL [INSTALL ...]
                        Install a repo
  -u, --update          Check for a system update and install it
  -r REMOVE [REMOVE ...], --remove REMOVE [REMOVE ...]
                        Remove a repo
  -c CONFIG [CONFIG ...], --config CONFIG [CONFIG ...]
                        Change a setting
```

#Hidden commands

These commands do not include help infomation

```
  --verbose
  --one-time-driver [ONE-TIME-DRIVER ...]
  --beta-gekyml (Currently disabled until 1.0.0 builds. Will be enabled in some beta builds)
```
